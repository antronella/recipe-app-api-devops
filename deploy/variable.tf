variable "prefix" {
  default = "raad"
  #recipe-app-api-devops 
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "falbocodes@gmail.com"
}