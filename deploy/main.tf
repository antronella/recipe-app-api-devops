terraform {
  backend "s3" {
    bucket         = "falbocodes-app-api-tfstate"
    key            = "recipe-app.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.54.0"
}

#A local value assigns a name to an expression
#var.prefix va prima creata e definita, per esempio in variable.tf
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  # qui sopra un esempio di interpolation syntax

  # i tag permettono di aggiungere metedata alla risorsa
  # tags applied to every resources we create in terraform
  common_tags = {
    #key-par
    Enviroment = terraform.workspace
    Project    = var.project
    Owner      = var.contact
    ManagedBy  = "Terraform"
  }
}

data "aws_region" "current" {


}