data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
    #recupera l'ami con questo nome
    #mettendo * facciamo in modo che scarichi la versione più recente
  }
  owners = ["amazon"]
  #prendiamo immagini ufficiali amazon
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  # soluzione uno
  #   tags = {
  #     Name = "${local.prefix}-bastion"
  #   }
  # soluzione due in cui eredita commont_tags (from main.tf) e aggiungi Name
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}